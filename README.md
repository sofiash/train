# Portfolio project IDATA1003 - 2023

STUDENT NAME = Sofia Simone Håbrekke 

STUDENT ID = 111692

## Project description

This project is an implementation of a train dispatch system that administers train departures in a 
24 hour window. The system allows a user to overview a schedule for train departures in this time period, 
and gives the option to view all coming trains, add a departure, edit the track given the train number (ID), 
add delays, remove train departures from system, search system for trains based on various criteria like train
number, destination and track. 

## Project structure

The project is organized in the following structure: 

src: the source code with the implementation of the TrainDeparture and TrainDepartureRegister class. 

test: the folder containing JUnit tests of the source code.

## Link to repository

https://gitlab.stud.idi.ntnu.no/sofiash/train


## How to run the project
To run the project, follow these steps:
- Open project in Java IDE like IntelliJ og Visual Studio Code
- Find the main class TrainDispatchApp, and run
- Follow instructions displayed on the terminal


## How to run the tests
To run the tests, follow these steps:
- Open project in Java IDE like IntelliJ og Visual Studio Code
- Navigate to the test folder
- Right-click the JUnit test classes and select "Run"


## References

ChatGPT is used for some javadocs. 
No other references was used in this project.  
Or if you have used code from a website or other source, include a link to the source.)
