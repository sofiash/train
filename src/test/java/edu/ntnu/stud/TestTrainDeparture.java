package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.DateTimeException;
import java.time.LocalTime;
import org.junit.jupiter.api.Test;

/**
 * <h1>Unit test for the {@link TrainDeparture} class.</h1>
 */

public class TestTrainDeparture {

  /**
   * <p>The instance of {@link TrainDeparture} used for testing.</p>
   */
  private final TrainDeparture trainDeparture =
      new TrainDeparture(LocalTime.of(8, 15), "L1", "Oslo", 100, 1);

  /**
   * <p>Test case to verify the successful creation of a {@link TrainDeparture} object using the constructor.</p>
   */

  @Test
  void createConstructorTest() {
    TrainDeparture newTrain = new TrainDeparture(LocalTime.of(0,0), "L4", "Asker", 101, 2);
    assertNotNull(newTrain);
  }
  /**
   * <p>Test case to verify that the TrainDeparture constructor throws a DateTimeException when provided
   * with an invalid LocalTime value. </p>
    */
  @Test
  void createConstructorTestThrowsLocalTime() {
    assertThrows(DateTimeException.class, () -> new TrainDeparture(LocalTime.of(25,0), "L4", "Asker", 101, 2));
  }
  /**
   * <p>Test case to verify that the TrainDeparture constructor throws an IllegalArgumentException when
   * provided with an empty line.</p>
   */
  @Test
  void createConstructorTestThrowsLine() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(2,0), "", "Asker", 101, 2));
  }
  /**
   * <p>Test case to verify that the TrainDeparture constructor throws an IllegalArgumentException when
   * provided with a null line.</p>
   */
  @Test
  void createConstructorTestThrowsLineNull() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(2,0), null, "Asker", 101, 2));
  }
  /**
   * <p>Test case to verify that the TrainDeparture constructor throws an IllegalArgumentException when
   * provided with an empty destination.</p>
   */
  @Test
  void createConstructorTestThrowsDestination() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(2,0), "R14", "", 101, 2));
  }
  /**
   * <p>Test case to verify that the TrainDeparture constructor throws an IllegalArgumentException when
   * provided with a null destination.</p>
   */
  @Test
  void createConstructorTestThrowsDestinationNull() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(2,0), "R14", null, 101, 2));
  }
  /**
   * <p>Test case to verify that the TrainDeparture constructor throws an IllegalArgumentException when
   * provided with a negative train number.</p>
   */
  @Test
  void createConstructorTestThrowsTrainNr() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(2,0), "R14", "Trondheim", -1, 2));
  }
  /**
   * <p>Test case to verify that the TrainDeparture constructor allows replacing a negative track number with a default value.</p>
   */
  @Test
  void createConstructorTestReplaceTrack() {
    new TrainDeparture(LocalTime.of(2,0), "R14", "Trondheim", 1, -7);
  }

  /**
   * <p>Test of the {@link TrainDeparture#getDelay()} method.</p>
   */
  @Test
  void getDelayTest(){
    assertEquals(trainDeparture.getDelay(), 0);
  }

  /**
   * <p>Test of the {@link TrainDeparture#setDelay(int)} method.</p>
   */
  @Test
  void setDelayTest() {
    assertTrue(trainDeparture.setDelay(35));
    assertEquals(trainDeparture.getDelay(), 35);
  }

  /**
   * <p>Tests that {@link TrainDeparture#setDelay(int)} will invalid input with 0.</p>
   */
  @Test
  void setDelayTestReplacesInvalid() {
    trainDeparture.setDelay(35);
    trainDeparture.setDelay(-3);
    assertEquals(0, trainDeparture.getDelay());
  }
  /**
   * <p>Tests that for the same delay, the {@link TrainDeparture#setTrack(int)} method returns false</p>
   */
  @Test
  void setDelayFalseIfSame() {
    assertFalse(trainDeparture.setDelay(0));
  }

  /**
   * <p>Test of the {@link TrainDeparture#setTrack(int)} method.</p>
   */
  @Test
  void setTrackTest() {
    assertTrue(trainDeparture.setTrack(4));
    assertEquals(trainDeparture.getTrack(), 4);
  }

  /**
   * <p>Tests that for invalid input, the {@link TrainDeparture#setTrack(int)} method sets the track to -1.</p>
   */
  @Test
  void testSetTrackReplacesInvalid() {
    trainDeparture.setTrack(-9);
    assertEquals(-1, trainDeparture.getTrack());
  }
  /**
   * <p>Tests that for the same track, the {@link TrainDeparture#setTrack(int)} method returns false</p>
   */
  @Test
  void setTrackFalseIfSame() {
    assertFalse(trainDeparture.setTrack(1));
  }

  /**
   * <p>Tests the {@link TrainDeparture#findTotalTime()} method.</p>
   */
  @Test
  void findTotalTimeTest() {
    assertEquals(trainDeparture.findTotalTime(), LocalTime.of(8, 15));
  }
  /**
   * <p>Tests the {@link TrainDeparture#toString()} method.</p>
   */
  @Test
  void testToString() {
    String expected = "08:15  | L1    | 100       | Oslo              |         |   1  ";
    String actual = trainDeparture.toString();

    assertEquals(expected, actual);
  }
}
