package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * <h1>This class provides tests for the {@link TrainDepartureRegister} class.</h1>
 */
public class TestTrainDepartureRegister {
  private final TrainDepartureRegister trainDepartureRegister =
      new TrainDepartureRegister();

  /**
   * <p>Sets up the initial state for the {@link TrainDepartureRegister} before each test.</p>
   */
  @BeforeEach
  void setUp() {
    trainDepartureRegister.addTrainDeparture(LocalTime.of(8, 15), "L1", "Oslo", 105, 1);
    trainDepartureRegister.addTrainDeparture(LocalTime.of(9, 30), "R11", "Bergen", 102, 2);
    trainDepartureRegister.addTrainDeparture(LocalTime.of(14, 20), "E12", "Kristiansand", 101, 2);
    trainDepartureRegister.addTrainDeparture(LocalTime.of(16, 40), "L14", "Oslo", 106, -1);
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#getTime()} method.</p>
   */
  @Test
  void getTimeTest() {
    LocalTime time = trainDepartureRegister.getTime();
    assertEquals(time, LocalTime.of(0, 0));
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#editTrack(int, int)} method.</p>
   */
  @Test
  void editTrackTest() {
    int trainNr = 101;
    int track = 3;

    assertTrue(trainDepartureRegister.editTrack(trainNr, track));

    int newTrack = trainDepartureRegister.findTrainDeparture(trainNr).getTrack();
    assertEquals(newTrack, track);

  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#editTrack(int, int)} wont set tracks for trains that dont exist</p>
   */
  @Test
  void editTrackTestReturnsFalse() {
    assertFalse(trainDepartureRegister.editTrack(1, 2));
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#editTrack(int, int)}
   *  will throw an IllegalArgumentException for invalid train number.</p>
   */
  @Test
  void editTrackTestThrowsInvalidTrainNr() {
    assertFalse(trainDepartureRegister.editTrack(-1, 2));
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#editTrack(int, int)}
   * will replace invalid input with -1.</p>
   */
  @Test
  void editTrackTestReplaceInvalid() {
    trainDepartureRegister.editTrack(101, -5);
    assertEquals(trainDepartureRegister.findTrainDeparture(101).getTrack(), -1);
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#addTrainDeparture(TrainDeparture)} method.</p>
   */
  @Test
  void addTrainDepartureTest() {
    TrainDeparture newDeparture =
        new TrainDeparture(LocalTime.of(10, 30), "R14", "Oslo", 104, 0);

    assertTrue(trainDepartureRegister.addTrainDeparture(newDeparture));
    assertTrue(trainDepartureRegister.registerContainsTrain(104));
  }

  /**
   * <p>Negative test for the {@link TrainDepartureRegister#addTrainDeparture(TrainDeparture)} method.</p>
   */
  @Test
  void addTrainDepartureTestReturnsFalse() {
    // train number 101 already exists in system
    TrainDeparture newDeparture =
        new TrainDeparture(LocalTime.of(10, 30), "R14", "Oslo", 101, 1);

    assertFalse(trainDepartureRegister.addTrainDeparture(newDeparture));
    assertEquals(trainDepartureRegister.getDepartures().size(), 4);

  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#removeDeparture(int)} method.</p>
   */
  @Test
  void removeDepartureTest() {

    assertTrue(trainDepartureRegister.removeDeparture(101));
    assertEquals(trainDepartureRegister.getDepartures().size(), 3);
  }

  /**
   * <p>Negative test for the {@link TrainDepartureRegister#removeDeparture(int)} method.</p>
   */
  @Test
  void removeDepartureTestReturnsFalse() {
    assertFalse(trainDepartureRegister.removeDeparture(104));
    assertEquals(trainDepartureRegister.getDepartures().size(), 4);
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#setDelay(int, int)} method.</p>
   */
  @Test
  void setDelayTest() {
    assertTrue(trainDepartureRegister.setDelay(101, 47));

    int delay = trainDepartureRegister.getDepartures().get(101).getDelay();
    assertEquals(47, delay);
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#setDelay(int, int)} will set 0 for invalid input.</p>
   */
  @Test
  void setDelayTestReplacesInvalid() {
    trainDepartureRegister.setDelay(101, -7);

    assertEquals(0, trainDepartureRegister.findTrainDeparture(101).getDelay());
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#setDelay(int, int)}
   * will throw an IllegalArgumentException for invalid train number.</p>
   */
  @Test
  void setDelayTestThrowsInvalidTrainNr() {
    assertFalse(trainDepartureRegister.setDelay(-1, 2));
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#setDelay(int, int)} won't set delay for a train with a train number that doesn't exist.</p>
   */
  @Test
  void setDelayTestReturnsFalse() {
    assertFalse(trainDepartureRegister.setDelay(104, 2));
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#setDepartures(HashMap)} method </p>
   */
  @Test
  void setDeparturesTest() {
    HashMap<Integer, TrainDeparture> newDepartures = new HashMap<>();

    trainDepartureRegister.setDepartures(newDepartures);
    assertTrue(trainDepartureRegister.getDepartures().isEmpty());
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#findTrainDeparture(int)} method.</p>
   */
  @Test
  void findTrainDepartureTest() {
    TrainDeparture foundTrain = trainDepartureRegister.findTrainDeparture(101);

    assertEquals(101, foundTrain.getTrainNr());
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#findDeparturesByDestination(String)} will throw an IllegalArgumentException
   * if no trains have that train number.</p>
   */
  @Test
  void findTrainDepartureTestThrowsInvalid() {
    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.findTrainDeparture(104));
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#findDeparturesByDestination(String)} method.</p>
   */
  @Test
  void findDeparturesByDestinationTest() {
    HashMap<Integer, TrainDeparture> trainsToOslo =
        trainDepartureRegister.findDeparturesByDestination("Oslo");

    assertTrue(trainsToOslo.containsKey(105));
    assertTrue(trainsToOslo.containsKey(106));
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#findDeparturesByDestination(String)} will throw an IllegalArgumentException
   * if no trains are going to the destination.</p>
   */
  @Test
  void findDeparturesByDestinationTestThrowsInvalid() {
    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.findDeparturesByDestination("San Diego"));
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#findDeparturesByTrack(int)} method.</p>
   */
  @Test
  void testFindDeparturesByTrack() {
    HashMap<Integer, TrainDeparture> trainsAtTrack2 =
        trainDepartureRegister.findDeparturesByTrack(2);

    assertTrue(trainsAtTrack2.containsKey(102));
    assertTrue(trainsAtTrack2.containsKey(101));
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#findDeparturesByTrack(int)} will throw an IllegalArgumentException
   * if no trains are arriving at that track</p>
   */
  @Test
  void findDeparturesByTrackTestThrowsInvalid() {
    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.findDeparturesByTrack(3));
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#findDeparturesInTime(LocalTime, LocalTime)} method.</p>
   */
  @Test
  void findDeparturesInTimeTest() {
    HashMap<Integer, TrainDeparture> trainsBetweenTimes =
        trainDepartureRegister.findDeparturesInTime(LocalTime.of(0, 0), LocalTime.of(10, 0));

    assertTrue(trainsBetweenTimes.containsKey(102));
    assertTrue(trainsBetweenTimes.containsKey(105));

  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#findDeparturesInTime(LocalTime, LocalTime)} will throw an IllegalArgumentException
   * if no trains are leaving in the given time interval.</p>
   */
  @Test
  void findDeparturesInTimeTestThrowsInvalid() {
    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.findDeparturesInTime(LocalTime.of(0, 0), LocalTime.of(8, 15)));
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#setTime(LocalTime)} method.</p>
   */
  @Test
  void setTimeTest() {
    trainDepartureRegister.setTime(LocalTime.of(10, 30));
    assertEquals(trainDepartureRegister.getTime(), LocalTime.of(10, 30));
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#setTime(LocalTime)} (String)} will throw a DateTimeException
   * if the time is invalid.</p>
   */
  @Test
  void setTimeTestThrowsInvalid() {
    assertThrows(DateTimeException.class,
        () -> trainDepartureRegister.setTime(LocalTime.of(27, 59)));
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#editTime(LocalTime)} method.</p>
   */
  @Test
  void editTimeTest() {
    assertTrue(trainDepartureRegister.editTime(LocalTime.of(10, 30)));
    assertEquals(LocalTime.of(10, 30), trainDepartureRegister.getTime());
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#editTime(LocalTime)} will not change time
   * to an earlier time than what is already defined.</p>
   */
  @Test
  void editTimeTestReturnsFalse() {
    trainDepartureRegister.editTime(LocalTime.of(10, 0));

    assertFalse(trainDepartureRegister.editTime(LocalTime.of(9, 30)));
    assertEquals(LocalTime.of(10, 0), trainDepartureRegister.getTime());
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#removeDepartedTrains(HashMap)} method.</p>
   */
  @Test
  void removeDepartedTrainsTest() {
    trainDepartureRegister.editTime(LocalTime.of(10, 0));
    HashMap<Integer, TrainDeparture> trainDepartures =
        trainDepartureRegister.removeDepartedTrains(trainDepartureRegister.getDepartures());

    assertTrue(trainDepartures.containsKey(101));
    assertTrue(trainDepartures.containsKey(106));
    assertFalse(trainDepartures.containsKey(105));
    assertFalse(trainDepartures.containsKey(102));
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#removeDepartedTrains(HashMap)} throws list if its empty..</p>
   */
  @Test
  void removeDepartedTrainsTestThrowsEmptyList() {
    trainDepartureRegister.editTime(LocalTime.of(23, 59));

    assertThrows(IllegalArgumentException.class, () ->
        trainDepartureRegister.removeDepartedTrains(trainDepartureRegister.getDepartures()));
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#sortList(HashMap)} (int)} method.</p>
   */
  @Test
  void sortListTest() {
    HashMap<Integer, TrainDeparture> sortedList =
        trainDepartureRegister.sortList(trainDepartureRegister.getDepartures());

    List<TrainDeparture> trainDepartures = sortedList.values().stream().toList();

    StringBuilder result = new StringBuilder();
    for (int i = 0; i < sortedList.size(); i++) {
      result.append(trainDepartures.get(i).toString()).append("\n");
    }
    String resultString = result.toString();
    assertEquals("""
        08:15  | L1    | 105       | Oslo              |         |   1 \s
        09:30  | R11   | 102       | Bergen            |         |   2 \s
        14:20  | E12   | 101       | Kristiansand      |         |   2 \s
        16:40  | L14   | 106       | Oslo              |         |     \s
        """, resultString);
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#findNextDeparture()} ()} method.</p>
   */
  @Test
  void getNextDepartureTest() {
    trainDepartureRegister.editTime(LocalTime.of(10, 0));
    TrainDeparture nextTrain = trainDepartureRegister.findNextDeparture();

    assertEquals(101, nextTrain.getTrainNr());
  }

  /**
   * <p>Tests that {@link TrainDepartureRegister#findNextDeparture()} will throw an IllegalArgumentException
   * if there are no more trains that day.</p>
   */
  @Test
  void getNextDepartureTestThrowsNone() {
    trainDepartureRegister.setTime(LocalTime.of(23, 59));
    assertThrows(IllegalArgumentException.class, trainDepartureRegister::findNextDeparture);
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#toString(int)} method.</p>
   */

  @Test
  void toStringTest() {
    String output = trainDepartureRegister.toString(102);
    String expectedOutput = "09:30  | R11   | 102       | Bergen            |         |   2  ";

    assertEquals(expectedOutput, output);
  }

  /**
   * <p>Tests the {@link TrainDepartureRegister#timeTable()} method.</p>
   */
  @Test
  void timeTableTest() {
    trainDepartureRegister.setDelay(101, 90);
    String expected = """
        DEPARTURES                                                 00:00
        -----------------------------------------------------------------
        Time   | Line  | Train nr. | Destination       | Delay   | Track
        -----------------------------------------------------------------
        08:15  | L1    | 105       | Oslo              |         |   1 \s
        09:30  | R11   | 102       | Bergen            |         |   2 \s
        14:20  | E12   | 101       | Kristiansand      | + 01:30 |   2 \s
        16:40  | L14   | 106       | Oslo              |         |     \s

        """;

    assertEquals(expected, trainDepartureRegister.timeTable());
  }
}
