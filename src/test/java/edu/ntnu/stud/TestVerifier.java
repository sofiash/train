package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalTime;
import java.util.HashMap;
import org.junit.jupiter.api.Test;

public class TestVerifier {
  private final Verifier verifier = new Verifier();
  /**
   * <h1> Tests for the Verifier class </h1>
   */
  @Test
  void verifyStringTest() {
    assertEquals("Hello world!", verifier.verifyString("Hello world!", "Message"));
  }

  /**
   * <p>Negative test for verify string class.</p>
   */

  @Test
  void verifyStringTestThrowsInvalid() {
    assertThrows(IllegalArgumentException.class, () -> verifier.verifyString(null, "Message"));
    assertThrows(IllegalArgumentException.class, () -> verifier.verifyString("", "Message"));
  }

  /**
   * <p> Tests the {@link Verifier#verifyInt(int, String)} method</p>
   */
  @Test
  void verifyIntTest() {
    int actual = verifier.verifyInt(3, "Number");
    assertEquals(3, actual);
  }
  /**
   * <p> Tests the {@link Verifier#verifyInt(int, String)} method throws invalid input.</p>
   */
  @Test
  void testVerifyIntThrowsInvalid() {
    assertThrows(IllegalArgumentException.class, () -> verifier.verifyInt(-3, "Number"));
  }

  /**
   * <p> Tests the {@link Verifier#verifyInt(int, String, int, int)} method throws invalid input.</p>
   */
  @Test
  void verifyInt2ndMethodTest() {
    int actual = verifier.verifyInt(3, "Number", 3, 10);
    assertEquals(3, actual);
  }

  /**
   * <p> Tests the {@link Verifier#verifyInt(int, String, int, int)} method throws invalid input.</p>
   */
  @Test
  void verifyIntThrowsInvalid() {
    assertThrows(IllegalArgumentException.class, () -> {
      verifier.verifyInt(11, "Number", 5, 10);
      verifier.verifyInt(2, "Number", 5, 10);
    });
  }

  /**
   * <p> Tests the {@link Verifier#verifyMapIsNotEmpty(HashMap, String)} method throws invalid input.</p>
   */
  @Test
  void verifyMapIsNotEmptyTest() {
    HashMap<Integer, TrainDeparture> list = new HashMap<>();
    list.put(100, new TrainDeparture(LocalTime.of(8, 15), "L1", "Oslo", 100, 1));
    list.put(101, new TrainDeparture(LocalTime.of(8, 0), "L4", "Trondheim", 101, 1));

    HashMap<Integer, TrainDeparture> other = verifier.verifyMapIsNotEmpty(list, "Departures");
    assertTrue(other.containsKey(100));
    assertTrue(other.containsKey(101));
  }

  /**
   * Test cases for verifying that the {@link Verifier#verifyMapIsNotEmpty(HashMap, String)} method throws
   * {@link IllegalArgumentException} for an empty map.
   */
  @Test
  void verifyHashMapThrowsEmpty() {
    HashMap<Integer, TrainDeparture> list = new HashMap<>();

    assertThrows(IllegalArgumentException.class, () -> verifier.verifyMapIsNotEmpty(list, "Departures"));
  }
  /**
   * <p>Test cases for verifying the functionality of the {@link Verifier#verifyLocalTimeInput(String)} method.</p>
   */
  @Test
  void verifyInputIsLocalTimeTest() {
    String input = "12:27";
    LocalTime actual = verifier.verifyLocalTimeInput(input);

    assertEquals(LocalTime.of(12,27), actual);
  }
  /**
   * <p>Test cases for verifying that the {@link Verifier#verifyLocalTimeInput(String)} method throws </p>
   * {@link IllegalArgumentException} for an invalid input string.
   */
  @Test
  void verifyInputIsLocalTimeThrows() {
    String input = "1227";

    assertThrows(IllegalArgumentException.class, () -> verifier.verifyLocalTimeInput(input));
  }
  /**
   * <p>Test cases for verifying the functionality of the {@link Verifier#verifyTrainDeparture(TrainDeparture)} method.</p>
   */
  @Test
  void verifyTrainDepartureTest() {
    TrainDeparture train = new TrainDeparture(LocalTime.of(10,20), "E12", "Asker", 123, 1);

    assertEquals(train, verifier.verifyTrainDeparture(train));
  }
  /**
   * <p>Test cases for verifying that the {@link Verifier#verifyTrainDeparture(TrainDeparture)} method throws
   * {@link IllegalArgumentException} for a null input.</p>
   */
  @Test
  void verifyTrainDepartureTestThrows () {
    TrainDeparture trainDeparture = null;

    assertThrows(IllegalArgumentException.class, () -> verifier.verifyTrainDeparture(trainDeparture));
  }

  /**
   * <p>Test cases for verifying the functionality of the {@link Verifier#replaceIfInvalid(int, int)} method.</p>
   */
  @Test
  void replaceIfInvalidTest() {
    int actual = verifier.replaceIfInvalid(-7, -1);
    assertEquals(-1, actual);
  }

  /**
   * <p>Test cases for verifying that the {@link Verifier#replaceIfInvalid(int, int)} method returns the
   * correct value for a valid input.</p>
   */
  @Test
  void replaceIfInvalidReturnsCorrect() {
    int actual = verifier.replaceIfInvalid(7, -1);
    assertEquals(7, actual);
  }
  /**
   * <p>Test cases for verifying the functionality of the {@link Verifier#replaceIfInvalid(String, int)} method
   * when the input is a string representing an integer.</p>
   */
  @Test
  void replaceIfInvalidInputTest() {
    int actual = verifier.replaceIfInvalid("10:20",-1);
    assertEquals(-1, actual);
  }

  /**
   * <p>Test cases for verifying that the {@link Verifier#replaceIfInvalid(String, int)} method returns the
   * correct value for a valid input string representing an integer.</p>
   */
  @Test
  void replaceIfInvalidInputReturnsCorrect() {
    int actual = verifier.replaceIfInvalid("7",-1);
    assertEquals(7, actual);
  }
}