package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Scanner;

/**
 * <h1>UserInterface class handles the user interface for the train departure system.</h1>
 * <p>It includes methods for printing departures, editing departures, searching for trains,
 * updating time, and more.</p>
 */
public class UserInterface {
  // CASES
  private static final int PRINT_DEPARTURES = 1;
  private static final int EDIT_DEPARTURES = 2;
  private static final int ADD_DEPARTURE = 1;
  private static final int EDIT_TRACK = 2;
  private static final int EDIT_DELAY = 3;
  private static final int REMOVE_DEPARTURE = 4;
  public static final int SEARCH_DEPARTURES = 3;
  private static final int SEARCH_BY_TRAIN_NR = 1;
  private static final int SEARCH_BY_DEPARTURE = 2;
  private static final int SEARCH_BY_TRACK = 3;
  private static final int SEARCH_BETWEEN_TIMES = 4;
  private static final int GET_NEXT_DEPARTURE = 5;
  private static final int UPDATE_TIME = 4;
  private static final int EXIT = 5;
  Scanner readInfo;
  private TrainDepartureRegister trainDepartures;
  private Verifier verifier;
  private Input input;

  public UserInterface() {
  }

  /**
   * <p>Initializes the train departure system with test data.</p>
   */
  public void init() {
    this.readInfo = new Scanner(System.in);
    this.verifier = new Verifier();
    this.trainDepartures = new TrainDepartureRegister();
    this.input = new Input(readInfo);
  }

  /**
   * <p>Displays the main menu and prompts the user for a choice.</p>
   *
   * @return The user's menu choice.
   */
  public int menuChoice() {
    System.out.println("""
            
            ****** TRAIN DISPATCH SYSTEM ******
                        
            [1] Print departures
            [2] Edit departures in system
            [3] Search departures
            [4] Update time
            [5] Quit
                    
            Please enter number between 1 and 5
            """);

    return verifier.verifyInt(Integer.parseInt(readInfo.nextLine()), "Choice", 1, 5);

  }

  /**
   * <p>Initiates the main loop for the user interface, allowing users to interact with
   * the train departure system until they choose to exit.</p>
   */
  public void start() {
    trainDepartures.addTrainDeparture(LocalTime.of(8, 15), "L1", "Oslo", 105, 1);
    trainDepartures.addTrainDeparture(LocalTime.of(9, 30), "R11", "Bergen", 102, 2);
    trainDepartures.addTrainDeparture(LocalTime.of(14, 20), "E12", "Kristiansand", 101, 2);
    trainDepartures.addTrainDeparture(LocalTime.of(16, 40), "L14", "Oslo", 106, -1);

    boolean finished = false;
    while (!finished) {
      try {
        int menuChoice = this.menuChoice();
        switch (menuChoice) {
          case PRINT_DEPARTURES -> printDepartures();
          case EDIT_DEPARTURES -> editDepartures();
          case SEARCH_DEPARTURES -> searchDepartures();
          case UPDATE_TIME -> updateTime();
          case EXIT -> finished = true;
          default -> System.out.println("Error: Invalid choice");
        }
      } catch (IllegalArgumentException | NullPointerException e) {
        System.out.println("Error: " + e.getMessage());
      }
    }
  }

  /**
   * <p>Prints the list of departures in the train departure system.</p>
   */
  public void printDepartures() {
    System.out.println(trainDepartures.timeTable());
  }

  /**
   * <p>Displays the menu for the choice "Edit departures".</p>
   *
   * @return the users choice.
   */
  public int editChoice() {
    System.out.println("""
                
        ****** EDIT TRAIN(S) IN SYSTEM ******
                
        [1] Add departure
        [2] Edit track
        [3] Edit delay
        [4] Remove departure
        [5] Exit to home screen
                
        please enter number between 1 and 5.
                
        """);


    return verifier.verifyInt(Integer.parseInt(readInfo.nextLine()), "Choice", 1, 5);
  }

  /**
   * <p>Initiates the process of editing train departures based on the user's choice.</p>
   */
  public void editDepartures() {
    int choice = verifier.verifyInt(editChoice(), "Choice", 1, 5);
    switch (choice) {
      case ADD_DEPARTURE -> addDeparture();
      case EDIT_TRACK -> editTrack();
      case EDIT_DELAY -> editDelay();
      case REMOVE_DEPARTURE -> removeDeparture();
      default -> System.out.println("Returning to main menu... ");
    }
  }

  /**
   * <p>Adds a departure to the system with input from user.</p>
   */
  public void addDeparture() {
    System.out.println("***** ADD DEPARTURE *****\n");

    LocalTime departure =
        input.inputLocalTime("Enter the new trains departure time on the format HH:MM: ");
    int track = input.inputWithDummyValue("""
        Enter the track the train will be arriving to:
        For trains without track, enter -1""", -1);
    String line = input.inputString("Enter the new trains line: ", "Line");
    String destination =
        input.inputString("Enter the new trains final destination: ", "Destination");
    int trainNr = input.inputInt("Enter the train number of the new train: ", "Train nr.");


    if (trainDepartures.addTrainDeparture(departure, line, destination, trainNr, track)) {
      System.out.println("Train was successfully added.");
    } else {
      System.out.println("Train could not be added due to duplicate train number");
    }
  }

  /**
   * <p>Assigns a track for a specific train based on users input.</p>
   */
  public void editTrack() {
    System.out.println("***** EDIT TRACK *****\n");

    int trainNr = input.inputInt("Enter the train number of the train you want to edit: ",
        "Train number");

    int track = input.inputWithDummyValue("""
        Enter the track the train will be arriving to:
        For trains without track, enter -1""", -1);


    if (trainDepartures.editTrack(trainNr, track)) {
      System.out.println("Track was successfully changed to: " + track);
    } else {
      if (!trainDepartures.registerContainsTrain(trainNr)) {
        System.out.println("Train with given train number does not exist");
      }
      System.out.println("Train could not be changed.");
    }
  }

  /**
   * <p>Edits delay for a specific train based on users input.</p>
   */

  public void editDelay() {
    System.out.println("***** EDIT DELAY *****\n");

    int trainNr = input.inputInt("Enter the train number of the train you want to edit: ",
        "Train number");
    int delay = input.inputLocalTime("Enter delay on the format HH:MM").toSecondOfDay() / 60;

    if (trainDepartures.setDelay(trainNr, delay)) {
      System.out.println(
          "Delay is set to: " + LocalTime.of(0, 0).plusMinutes(delay).toString());
    } else {
      if (trainDepartures.registerContainsTrain(trainNr)) {
        System.out.println("System doesn't include this train number.");
      }
      System.out.println("Delay could not be changed.");
    }
  }

  /**
   * <p>Removes a specific departure based on input from user.</p>
   */
  public void removeDeparture() {
    System.out.println("***** REMOVE DEPARTURE FROM SYSTEM ****\n");

    int trainNr = input.inputInt("Enter the train number of the train you want to delete: ",
        "Train number");

    System.out.println(trainDepartures.toString(trainNr));
    String confirmation =
        input.inputString("Are you sure you want to delete this train? (yes/no) ",
            "Confirmation");

    if (confirmation.equals("yes")) {
      if (trainDepartures.removeDeparture(trainNr)) {
        System.out.println("Train deleted successfully");
      } else {
        System.out.println("Train could not be deleted.");
      }
    } else {
      System.out.println("Train could not be deleted. ");
    }
  }

  /**
   * <p>Displays the menu for the choice "Search departures".</p>
   *
   * @return users choice.
   */

  public int searchChoice() {
    System.out.println(
        """
            ***** SEARCH DEPARTURES IN SYSTEM ******
                    
            [1] Search by train number
            [2] Search by destination
            [3] Search by track
            [4] Search for departures in a time interval
            [5] Find next departure
            [6] Exit to main menu
                    
            Please enter number between 1 and 6
            """);

    return verifier.verifyInt(Integer.parseInt(readInfo.nextLine()), "Choice", 1, 6);
  }

  /**
   * <p>Initiates the process of searching train departures based on the users choice.</p>
   */
  public void searchDepartures() {
    int choice = searchChoice();
    switch (choice) {
      case SEARCH_BY_TRAIN_NR -> searchByTrainNr();
      case SEARCH_BY_DEPARTURE -> searchByDestination();
      case SEARCH_BY_TRACK -> searchByTrack();
      case SEARCH_BETWEEN_TIMES -> searchBetweenTimes();
      case GET_NEXT_DEPARTURE -> getNextDeparture();
      default -> System.out.println("Returning to main menu... ");
    }
  }

  /**
   * <p>Searches the train departures for a specific train based on a train number,
   * given by users input.</p>
   */

  public void searchByTrainNr() {
    System.out.println("***** SEARCH TRAINS BASED ON TRAIN NUMBER *****\n");

    int trainNr = input.inputInt("Enter the train number of the train you want to find: ",
        "Train number");
    if (trainDepartures.registerContainsTrain(trainNr)) {
      System.out.println(trainDepartures.toString(trainNr));
    } else {
      System.out.println("Train could not be found.");
    }
  }

  /**
   * <p>Searches for train departures based on the trains' destination.</p>
   */
  public void searchByDestination() {
    System.out.println("***** SEARCH TRAINS BASED ON DESTINATION *****\n");

    String destination =
        input.inputString("Enter the destination of the train(s) you want to find: ",
            "Destination");

    trainDepartures.findDeparturesByDestination(destination)
        .forEach((key, value) -> System.out.println(trainDepartures.toString(key)));
  }

  /**
   * <p>Searches for train departures based on the track they will be on, based on users input.</p>
   */

  public void searchByTrack() {
    System.out.println("****** SEARCH DEPARTURES BASED ON TRACK ******\n");

    int track = input.inputWithDummyValue("""
        Enter a track for the train(s) you want to search for:
        search for -1 for trains without a track.""", -1);

    String trackString = Integer.toString(track);
    if (track == -1) {
      trackString = "empty";
    }

    System.out.println("Searching for trains arriving at track: " + trackString);
    trainDepartures.findDeparturesByTrack(track)
        .forEach((key, value) -> System.out.println(trainDepartures.toString(key)));
  }

  /**
   * <p>Searches for trains between two times, given users input.</p>
   */
  public void searchBetweenTimes() {
    System.out.println("****** SEARCH TRAINS DEPARTING IN A TIME INTERVAL ******\n");

    LocalTime startTime = input.inputLocalTime("Enter start time in the format HH:MM: ");
    LocalTime endTime = input.inputLocalTime("Enter end time in the format HH:MM: ");

    trainDepartures.findDeparturesInTime(startTime, endTime)
        .forEach((key, value) -> System.out.println(trainDepartures.toString(key)));
  }

  /**
   * <p>Finds and prints the next train departure, based on time of departure.</p>
   */
  public void getNextDeparture() {
    System.out.println("***** FIND NEXT DEPARTURE TO DEPART STATION ***** \n");

    TrainDeparture trainDeparture = trainDepartures.findNextDeparture();
    System.out.println(trainDeparture.toString());
  }

  /**
   * <p>Updates the system clocks time, based on input from user.</p>
   */

  public void updateTime() {
    System.out.println("****** UPDATE TIME ****** \n");

    LocalTime newTime = input.inputLocalTime("Enter time in the format HH:MM: ");

    if (trainDepartures.editTime(newTime)) {
      System.out.println("""
          Time was successfully changed.
          Current time:""" + trainDepartures.getTime().toString());
    } else {
      System.out.println(
          "Time could not be changed because new time is earlier than original time. ");
    }
  }
}