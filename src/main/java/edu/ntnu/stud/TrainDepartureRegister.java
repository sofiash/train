package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <h1>Manages and provides functionality for train departures in a train departure system.</h1>
 *
 * @see TrainDeparture
 * @see Verifier
 */
public class TrainDepartureRegister {
  private HashMap<Integer, TrainDeparture> departures;
  private LocalTime time;
  private final Verifier verifier = new Verifier();

  /**
   * <p>Constructs a TrainDepartureSystem with a list of train departures and the current time.</p>
   */
  public TrainDepartureRegister() {
    setTime(LocalTime.of(0, 0));
    setDepartures(new HashMap<>());
  }

  public void setTime(LocalTime newTime) {
    this.time = newTime;
  }

  /**
   * <p>Gets the current time of the system.</p>
   *
   * @return The current time.
   */
  public LocalTime getTime() {
    return time;
  }

  /**
   * <p>Sets the track number for a specific train.</p>
   *
   * @param trainNr The train number.
   * @param track   The new track number.
   * @return true if successful, false otherwise.
   */
  public boolean editTrack(int trainNr, int track) {
    try {
      return findTrainDeparture(verifier.verifyInt(trainNr, "Train number")).setTrack(
          verifier.replaceIfInvalid(track, -1));
    } catch (IllegalArgumentException e) {
      return false;
    }
  }


  /**
   * <p>Adds a new train departure to the system
   * if a train with the same number doesn't already exist.</p>
   *
   * @param departure The new train departure.
   * @return true if added, false if train with the same number already exists.
   */
  public boolean addTrainDeparture(TrainDeparture departure) {
    int trainNr = departure.getTrainNr();

    if (registerContainsTrain(trainNr)) {
      return false;
    } else {
      departures.put(verifier.verifyInt(trainNr, "Train number"), new TrainDeparture(departure));
      return true;
    }
  }

  /**
   * <p>Adds a new train departure to the collection based on the provided information.</p>
   *
   * @param departureTime The departure time of the train.
   * @param line          The line or route of the train.
   * @param destination   The destination of the train.
   * @param trainNr       The train number.
   * @param track         The track number.
   * @return {@code true} if the train departure was successfully added, {@code false} otherwise.
   */

  public boolean addTrainDeparture(LocalTime departureTime, String line,
                                   String destination, int trainNr, int track) {

    TrainDeparture departure = new TrainDeparture(departureTime, line, destination, trainNr, track);
    return addTrainDeparture(departure);
  }

  /**
   * <p>Removes train from system.</p>
   *
   * @param trainNr The train number.
   * @return true if removed, false if train not found.
   */

  public boolean removeDeparture(int trainNr) {
    if (registerContainsTrain(trainNr)) {
      departures.remove(
          verifier.verifyInt(trainNr, "Train number"));
      return true;
    } else {
      return false;
    }
  }

  /**
   * <p>Sets the delay for a specific train.</p>
   *
   * @param trainNr The train number.
   * @param delay   The new delay in minutes.
   * @return true if successful, false otherwise.
   */
  public boolean setDelay(int trainNr, int delay) {
    try {
      return findTrainDeparture(trainNr).setDelay(
          verifier.replaceIfInvalid(delay, 0));
    } catch (IllegalArgumentException e) {
      return false;
    }
  }

  /**
   * <p>Finds and returns a specific train departure based on the train number.</p>
   *
   * @param trainNr The train number to search for.
   * @return The TrainDeparture object if found, or null if not found.
   */
  public TrainDeparture findTrainDeparture(int trainNr) {
    return verifier.verifyTrainDeparture(departures.get(trainNr));
  }

  /**
   * <p>Checks if the register contains a train with the given number.</p>
   *
   * @param trainNr The train number to check.
   * @return true if the train is found, false otherwise.
   */

  public boolean registerContainsTrain(int trainNr) {
    return departures.containsKey(trainNr);
  }

  /**
   * <p>Gets the list of all train departures in the system.</p>
   *
   * @return The list of train departures.
   */
  public HashMap<Integer, TrainDeparture> getDepartures() {
    return departures;
  }

  /**
   * <p>Finds and returns a list of train departures based on the destination.</p>
   *
   * @param destination The destination to search for.
   * @return The list of TrainDeparture objects matching the destination.
   */
  public HashMap<Integer, TrainDeparture> findDeparturesByDestination(String destination) {
    verifier.verifyString(destination, "Destination");

    HashMap<Integer, TrainDeparture> foundDepartures = new HashMap<>();

    departures.forEach((key, value) -> {
      if (value.getDestination().equals(destination)) {
        foundDepartures.put(key, new TrainDeparture(value));
      }
    });
    return verifier.verifyMapIsNotEmpty(sortList(foundDepartures),
        "Departures going to " + destination);
  }

  /**
   * <p>Finds and returns a list of train departures based on the track.</p>
   *
   * @param track The track to search for.
   * @return The list of TrainDeparture objects matching the track.
   */
  public HashMap<Integer, TrainDeparture> findDeparturesByTrack(int track) {
    verifier.replaceIfInvalid(track, -1);

    HashMap<Integer, TrainDeparture> foundDepartures = new HashMap<>();

    departures.forEach((key, value) -> {
      if (value.getTrack() == track) {
        foundDepartures.put(key, new TrainDeparture(value));
      }
    });
    return verifier.verifyMapIsNotEmpty(sortList(foundDepartures),
        "Departures arriving at track " + track);
  }

  /**
   * <p>Finds and returns a list of train departures
   * that depart between a start time and an end time.</p>
   *
   * @param startTime The earliest time to search for.
   * @param endTime   is the latest time to search for.
   * @return The list of TrainDeparture objects matching the track.
   */
  public HashMap<Integer, TrainDeparture> findDeparturesInTime(LocalTime startTime,
                                                               LocalTime endTime) {
    HashMap<Integer, TrainDeparture> foundDepartures = new HashMap<>();

    departures.forEach((key, value) -> {
      if (value.findTotalTime().isAfter(startTime) && value.findTotalTime().isBefore(endTime)) {
        foundDepartures.put(key, new TrainDeparture(value));
      }
    });
    return verifier.verifyMapIsNotEmpty(sortList(foundDepartures),
        "departures between " + startTime.toString() + " and " + endTime.toString());
  }

  /**
   * <p>Sets the new time for the system if it is later than the current time.</p>
   *
   * @param newTime The new time.
   * @return true if the time is updated, false otherwise.
   */
  public boolean editTime(LocalTime newTime) {
    if (newTime.isAfter(time)) {
      setTime(newTime);
      return true;
    } else {
      return false;
    }
  }

  /**
   * <p>Removes train departures that have already departed based on the current time.</p>
   *
   * @param list The list of train departures to filter.
   * @return A filtered list of train departures that haven't departed yet.
   */
  public HashMap<Integer, TrainDeparture> removeDepartedTrains(
      HashMap<Integer, TrainDeparture> list) {
    HashMap<Integer, TrainDeparture> filteredList = new HashMap<>();

    list.forEach((key, value) -> {
      if (value.findTotalTime().isAfter(time)) {
        filteredList.put(key, new TrainDeparture(value));
      }
    });
    return verifier.verifyMapIsNotEmpty(sortList(filteredList), "List of departures");
  }

  /**
   * <p>Sorts list by departure time (including delay).</p>
   *
   * @param departureList a HashMap list of departures that will be sorted.
   * @return A sorted HashMap of departureList.
   */
  public HashMap<Integer, TrainDeparture> sortList(HashMap<Integer, TrainDeparture> departureList) {

    List<TrainDeparture> trainDepartures = new ArrayList<>(departureList.values());

    trainDepartures.sort(Comparator.comparing(TrainDeparture::getDepartureTime));

    HashMap<Integer, TrainDeparture> sortedDepartures = new LinkedHashMap<>();

    trainDepartures.forEach(
        departure -> sortedDepartures.put(departure.getTrainNr(), new TrainDeparture(departure)));
    return verifier.verifyMapIsNotEmpty(sortedDepartures, "Departures");
  }

  /**
   * <p>Sets the list of departures.</p>
   *
   * @param trainDepartures The new list of departures.
   */

  public void setDepartures(HashMap<Integer, TrainDeparture> trainDepartures) {
    HashMap<Integer, TrainDeparture> newDepartures = new LinkedHashMap<>();

    for (Map.Entry<Integer, TrainDeparture> entry : trainDepartures.entrySet()) {
      newDepartures.put(entry.getKey(), new TrainDeparture(entry.getValue()));
    }
    this.departures = newDepartures;
  }

  /**
   * <p>Finds the next departure.</p>
   *
   * @return the next train to depart the station.
   */

  public TrainDeparture findNextDeparture() {
    HashMap<Integer, TrainDeparture> comingDepartures =
        verifier.verifyMapIsNotEmpty(removeDepartedTrains(departures), "Departures");

    List<TrainDeparture> trains = comingDepartures.values().stream().toList();

    return new TrainDeparture(verifier.verifyTrainDeparture(trains.get(0)));
  }

  public String toString(int trainNr) {
    return new TrainDeparture(findTrainDeparture(trainNr)).toString();
  }

  /**
   * <p>Generates a formatted timetable string for the train departures in the system.</p>
   *
   * @return The formatted timetable string.
   */
  public String timeTable() {

    //HEADING
    StringBuilder string = new StringBuilder(String.format("%" + -59 + "s", "DEPARTURES"));
    string.append(time);
    string.append("\n").append("-".repeat(65));
    string.append("\n")
        .append(String.format(
            "%-6s%-8s%-12s%-20s%-10s%-8s",
            "Time",
            " | Line",
            " | Train nr.",
            " | Destination",
            " | Delay",
            " | Track"
        ));
    string.append("\n").append("-".repeat(65));

    HashMap<Integer, TrainDeparture> displayedDepartures = removeDepartedTrains(departures);
    sortList(displayedDepartures);

    for (TrainDeparture train : displayedDepartures.values()) {
      string.append("\n").append(train.toString());
    }
    string.append("\n\n");
    return string.toString();
  }
}
