package edu.ntnu.stud;

/**
 *
 * <h1>The main class for running the train departure system client program.</h1>
 */

public class TrainDispatchApp {
  /**
   * <p>Main method to start the train departure system client program.</p>
   *
   * @param args Command line arguments (not used).
   */
  public static void main(String[] args) {
    UserInterface user = new UserInterface();
    user.init();
    user.start();
  }
}
