package edu.ntnu.stud;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;

/**
 * <h1>A utility class for verifying input values.</h1>
 */

class Verifier {
  /**
   * <p>Default constructor for the Verifier class.</p>
   */
  public Verifier() {
  }

  /**
   * <p>Verifies if the input string is valid.</p>
   *
   * @param inputToCheck   The String value to be verified.
   * @param typeForMessage A String representing the type of the input for use in the message.
   * @return The String value if it is valid.
   * @throws IllegalArgumentException if the input is invalid or empty.
   */
  public String verifyString(String inputToCheck, String typeForMessage) {
    if (inputToCheck == null || inputToCheck.isEmpty()) {
      throw new IllegalArgumentException(typeForMessage + " must include text");
    }
    return inputToCheck;
  }

  /**
   * <p>Verifies if the input integer is within a specified range.</p>
   *
   * @param inputToCheck   The integer value to be verified.
   * @param typeForMessage A String representing the type of the input for use in the error message.
   * @param lowerLimit     The lower limit of the allowed range (inclusive).
   * @param upperLimit     The upper limit of the allowed range (inclusive).
   * @return The input integer value if it is within the specified range.
   * @throws IllegalArgumentException If the input value is outside the specified range.
   */

  public int verifyInt(int inputToCheck, String typeForMessage, int lowerLimit, int upperLimit) {
    if (inputToCheck > upperLimit || inputToCheck < lowerLimit) {
      throw new IllegalArgumentException(typeForMessage + " is invalid");
    }
    return inputToCheck;
  }

  /**
   * <p>Verifies if the input integer is above or equal to 0.</p>
   *
   * @param input          The integer value to be verified.
   * @param typeForMessage A String representing the type of the input for use in the error message.
   * @return The input integer value if it is within the specified range.
   * @throws IllegalArgumentException If the input value is outside the specified range.
   */

  public int verifyInt(int input, String typeForMessage) {
    if (input < 1) {
      throw new IllegalArgumentException(typeForMessage + " is invalid");
    }
    return input;
  }

  /**
   * <p>Replaces the input value with a dummy value if it is invalid.
   * The method checks if the value
   * is less than 1, and returns the specified dummy value.</p>
   *
   * @param input      The input value to be verified.
   * @param dummyValue The value to be returned if the input is invalid.
   * @return The verified input value or the specified dummy value if verification fails.
   */

  public int replaceIfInvalid(int input, int dummyValue) {
    if (input < 1) {
      return dummyValue;
    } else {
      return input;
    }
  }

  /**
   * <p>Replaces the input value with a dummy value if it is invalid.</p>
   * This method attempts to verify the input as an integer using the {@code verifyInt} method.
   * If the verification fails and an {@code IllegalArgumentException} is thrown,
   * the method returns the specified dummy value.
   *
   * @param input      The input value to be verified.
   * @param dummyValue The value to be returned if the input is invalid.
   * @return The verified input value or the specified dummy value if verification fails.
   */
  public int replaceIfInvalid(String input, int dummyValue) {
    try {
      return Integer.parseInt(input);
    } catch (IllegalArgumentException e) {
      return dummyValue;
    }
  }

  /**
   * <p>Verifies the non-empty nature of a HashMap.</p>
   *
   * @param list           The HashMap to be verified.
   * @param typeForMessage A String representing the type of the values for use in the
   *                       error message.
   * @return The input HashMap if it is not empty.
   * @throws IllegalArgumentException If the HashMap is empty.
   */

  public HashMap<Integer, TrainDeparture> verifyMapIsNotEmpty(
      HashMap<Integer, TrainDeparture> list,
      String typeForMessage) {
    if (list.isEmpty()) {
      throw new IllegalArgumentException("List of "
          + typeForMessage + " is empty. ");
    } else {
      return list;
    }
  }

  /**
   * <p>Parses the provided string representation of time and returns the
   * corresponding LocalTime object.</p>
   *
   * @param input A string representing time in the format expected by LocalTime.parse().
   * @return The parsed LocalTime object.
   * @throws IllegalArgumentException If the input string is not a valid representation of time.
   */

  public LocalTime verifyLocalTimeInput(String input) {
    try {
      return LocalTime.parse(input);
    } catch (DateTimeParseException e) {
      throw new IllegalArgumentException("Time is invalid.");
    }
  }

  /**
   * <p>Verifies the existence of a TrainDeparture object and returns it if it exists.</p>
   *
   * @param departure The TrainDeparture object to be verified.
   * @return The provided TrainDeparture object if it exists.
   * @throws IllegalArgumentException If the provided TrainDeparture object is null.
   */
  public TrainDeparture verifyTrainDeparture(TrainDeparture departure) {
    if (departure == null) {
      throw new IllegalArgumentException("Train does not exist");
    } else {
      return departure;
    }
  }
}