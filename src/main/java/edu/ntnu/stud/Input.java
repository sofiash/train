package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Scanner;

/**
 * <h1>Utility class for handling user input and performing validation.</h1>
 */
public class Input {

  private final Scanner scanner;
  private final Verifier verifier = new Verifier();

  /**
   * <p>Constructs an Input object with the provided Scanner.</p>
   *
   * @param scanner the Scanner object for reading user input
   */
  public Input(Scanner scanner) {
    this.scanner = scanner;
  }

  /**
   * <p>Takes user input and replaces it with a dummy value if the input is invalid.</p>
   *
   * @param message        The prompt message for the user.
   * @param dummyValue     The dummy value to return in case of invalid input.
   * @return The validated input or a dummy value if invalid.
   */
  public int inputWithDummyValue(String message, int dummyValue) {
    System.out.println(message);
    int input = verifier.replaceIfInvalid(scanner.nextLine(), dummyValue);
    return verifier.replaceIfInvalid(input, dummyValue);
  }

  /**
   * <p>Takes user input for an integer and validates it.</p>
   *
   * @param message        The prompt message for the user.
   * @param typeForMessage The type of input for error messages.
   * @return The validated integer input.
   */
  public int inputInt(String message, String typeForMessage) {
    do {
      System.out.println(message);
      try {
        int input = Integer.parseInt(scanner.nextLine());
        return verifier.verifyInt(input, typeForMessage);
      } catch (IllegalArgumentException e) {
        System.out.println("Input was invalid, try again.");
      }
    } while (true);
  }

  /**
   * <p>Takes user input for a string and validates it.</p>
   *
   * @param message        The prompt message for the user.
   * @param typeForMessage The type of input for error messages.
   * @return The validated string input.
   */
  public String inputString(String message, String typeForMessage) {
    do {
      System.out.println(message);
      try {
        String input = scanner.nextLine();
        return verifier.verifyString(input, typeForMessage);
      } catch (IllegalArgumentException e) {
        System.out.println("Error: " + e.getMessage());
      }
    } while (true);
  }

  /**
   * <p>Takes user input for a LocalTime and validates it.</p>
   *
   * @param message The prompt message for the user.
   * @return The validated LocalTime input.
   */
  public LocalTime inputLocalTime(String message) {
    do {
      System.out.println(message);
      try {
        return verifier.verifyLocalTimeInput(scanner.nextLine());
      } catch (IllegalArgumentException e) {
        System.out.println("Time input is invalid, please try again.");
      }
    } while (true);
  }
}
