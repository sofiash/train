package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * <h1>Represents information about a train departure.</h1>
 */

public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final String destination;
  private int track;
  private int delay;          // in minutes
  private final int trainNr;
  private final Verifier verifier = new Verifier();


  /**
   * <p>Constructs a TrainDeparture with specified details.</p>
   *
   * @param departureTime The departure time.
   * @param track         The track number.
   * @param line          The train line.
   * @param destination   The destination.
   * @param trainNr       The train number.
   */

  public TrainDeparture(LocalTime departureTime, String line,
                        String destination, int trainNr, int track) {
    this.departureTime = departureTime;
    this.line = verifier.verifyString(line, "Line");
    this.destination = verifier.verifyString(destination, "Destination");
    this.trainNr = verifier.verifyInt(trainNr, "Train number");
    setTrack(track);
    setDelay(0);
  }

  /**
   * <p>Constructs a TrainDeparture as a copy of another TrainDeparture instance.</p>
   *
   * @param trainDeparture The TrainDeparture instance to copy.
   */
  public TrainDeparture(TrainDeparture trainDeparture) {
    this.departureTime = trainDeparture.getDepartureTime();
    this.track = trainDeparture.getTrack();
    this.delay = trainDeparture.getDelay();
    this.line = trainDeparture.getLine();
    this.destination = trainDeparture.getDestination();
    this.trainNr = trainDeparture.getTrainNr();
  }

  /**
   * <p>Gets the delay in minutes.</p>
   *
   * @return The delay in minutes.
   */

  public int getDelay() {
    return delay;
  }

  /**
   * <p>Gets the train line.</p>
   *
   * @return The line.
   */

  public String getLine() {
    return line;
  }

  /**
   * <p>Gets the departure time.</p>
   *
   * @return The departure time.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * <p>Gets the track number.</p>
   *
   * @return The track number.
   */
  public int getTrack() {
    return track;
  }

  /**
   * <p>Gets the destination of the train.</p>
   *
   * @return The destination of the train.
   */

  public String getDestination() {
    return destination;
  }

  /**
   * <p>Gets the train number associated with this train.</p>
   *
   * @return The train number.
   */
  public int getTrainNr() {
    return trainNr;
  }

  /**
   * <p>Sets the delay in minutes.</p>
   *
   * @param newDelay The new delay in minutes.
   */
  public boolean setDelay(int newDelay) {
    if (delay != newDelay) {
      this.delay = verifier.replaceIfInvalid(newDelay, 0);
      return true;
    } else {
      return false;
    }
  }

  /**
   * <p>Sets the track number. </p>
   *
   * @param newTrack The new track number.
   */
  public boolean setTrack(int newTrack) {
    if (track != newTrack) {
      this.track = verifier.replaceIfInvalid(newTrack, -1);
      return true;
    } else {
      return false;
    }
  }

  /**
   * <p>Calculates and returns the total time including the delay.</p>
   *
   * @return The total time including the delay.
   */
  public LocalTime findTotalTime() {
    return departureTime.plusMinutes(delay);
  }

  /**
   * <p> Returns a string representation of the object.
   * The string includes formatted information about the total time, departure time,
   * line, train number, destination, and track.</p>
   *
   * @return A formatted string representing the object.
   */

  public String toString() {
    String delayString = " ";
    String trackString = Integer.toString(track);

    if (delay != 0) {
      delayString = "+ " + LocalTime.MIDNIGHT.plusMinutes(delay).toString();
    }
    if (track == -1) {
      trackString = " ";
    }

    return String.format(
        "%-6s%-8s%-12s%-20s%-10s%-8s",
        departureTime,
        " | " + line,
        " | " + trainNr,
        " | " + destination,
        " | " + delayString,
        " |   " + trackString
    );
  }
}
